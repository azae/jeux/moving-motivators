require 'squib'
require 'yaml'

Version=1
Copywright = "http://azae.net, CC~BY-SA~3.0~FR, version: v#{Version}"

def save_home_made(file)
  save format: :pdf, file: file, width: "29.7cm", height: "21cm", trim: 40, gap: 0
end

def debug_grid()
  grid width: 25,  height: 25,  stroke_color: '#659ae9', stroke_width: 1.5
  grid width: 100, height: 100, stroke_color: '#659ae9', stroke_width: 4
end

Cards = YAML.load_file('data/cards.yml')

Squib::Deck.new(cards: Cards.size, layout: 'layout/details.yml') do
  background color: 'white'
  rect layout: 'safe', fill_color: Cards.map { |e| e["color"]}, radius: 10
  rect layout: 'title-background'
  rect layout: 'description-background'
  svg file: Cards.map { |e| e["icon"]}, layout: 'icon'
  text str: Cards.map { |e| e["title"]}, layout: 'title'
  text str: Cards.map { |e| e["details"]}, layout: 'details'
  text str: Copywright, layout: 'copyright'
  save_home_made "cards.pdf"
end
