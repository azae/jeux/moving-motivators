[![pipeline status](https://gitlab.com/azae/games/moving-motivators/badges/master/pipeline.svg)](https://gitlab.com/azae/games/moving-motivators/commits/master)

# Moving motivators

[Cards](https://gitlab.com/azae/games/moving-motivators/builds/artifacts/master/browse/_output?job=squib).


## Build

    docker run --rm --user `id -u`:`id -g` -it -v $(pwd):/data -w /data registry.gitlab.com/azae/outils/squib:0.14.2 make


## Sources

https://management30.com/practice/moving-motivators/
